<?php
/**
 * Plugin Name: .SP No Login !!
 * Plugin URI: https://shapedplugin.com/
 * Description: Never authenticate, you're always the admin. Obviously for test/dev sites!
 * Author: Shamim Mia
 * Version: 1.1.0
 * Author URI: http://shamimmia.com/
 *
 * @package sp-nologin
 */

if ( ! function_exists( 'wp_validate_auth_cookie' ) ) {
	/**
	 * Authenticate cookie.
	 *
	 * @return statement
	 */
	function wp_validate_auth_cookie() {
		return 1;
	}
	add_action( 'admin_head', 'sp_nologin_admin_css' );
	add_action( 'wp_head', 'sp_nologin_admin_css' );
	add_action( 'wp_before_admin_bar_render', 'sp_nologin_custom_toolbar', 999 );
}

/**
 * Style for the admin toolbar.
 *
 * @return void
 */
function sp_nologin_admin_css() {
	echo '
    <style>
    #wp-admin-bar-debug-bar-no-login > .ab-item {
        color:#fff;
        font-weight:bold;
        background: #a73737;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to left, #7a2828, #a73737);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to left, #7a2828, #a73737); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

    }
    
    #wp-admin-bar-debug-bar-no-login .ab-submenu .ab-item{
        color:#f3f306;
        background: #134E5E;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #134E5E, #71B280);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #134E5E, #71B280); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

    }    
    </style>
    ';
}

/**
 * Text for the toolbar
 *
 * @since 1.0.0
 * @return void
 */
function sp_nologin_custom_toolbar() {
	global $wp_admin_bar;

	$args = array(
		'id'    => 'debug-bar-no-login',
		'title' => 'Super Admin!',
	);
	$wp_admin_bar->add_menu( $args );

	$args = array(
		'id'     => 'debug-bar-no-login-child',
		'parent' => 'debug-bar-no-login',
		'title'  => 'ShapedPlugin',
	);
	$wp_admin_bar->add_menu( $args );

}

